@extends('layouts.dashboard')


@section('body')
<div class="row">

  @foreach ($allImages as $image)
  <div class="col col-lg-4 col-sm-12 " >
    <div class="card" >
      {{-- {{ dd($image->filename) }} --}}
      {{-- <img src="/storage/2021-02-14T17.jpeg" class="card-img-top" alt="..."> --}}
      @if ($image->filetype == 'image')
      <img src="/storage/{{ $image->filename }}" style="" class="card-img-top" alt="...">
          
      @endif
      @if ($image->filetype == 'video')
      <video width="400" controls>
        <source src="/storage/{{ $image->filename }}" type="video/mp4">
        {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
        Your browser does not support HTML video.
      </video>
          {{-- <video src="/storage/{{ $image->filename }}"></video> --}}
      @endif
      <div class="card-body">
        <h5 class="card-title">Card title</h5>
      <form action="/media/waiting/delete" method="post">
          @csrf
          <input type="hidden" name="id" value="{{ $image->id }}">
          <input type="submit" value="delete">
      </form>
      <form action="/media/pushnow" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $image->id }}">
        <input type="submit" value="push message">
    </form>
  
        <p class="card-text">
          {{ $image->post }}
      </p>
      </div>
    </div>
  </div>
      
  @endforeach

</div>

@if ($allImages->lastPage() > 1)
      </div>
      <nav aria-label="Page navigation example">
        <ul class="pagination">
          <li class="page-item {{ ($allImages->currentPage() == 1) ? ' disabled' : '' }}">
            <a class="page-link" href="{{ $allImages->url(1) }}" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          @for ($i = 1; $i <= $allImages->lastPage(); $i++)
          <li class="page-item {{ ($allImages->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="{{ $allImages->url($i) }}">{{ $i }}</a>
          </li>
          @endfor
          <li class="page-item">
            <a class="page-link {{ ($allImages->currentPage() == $allImages->lastPage()) ? ' disabled' : '' }}" href="{{ $allImages->url($allImages->currentPage()+1) }}" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>

      @endif


@endsection