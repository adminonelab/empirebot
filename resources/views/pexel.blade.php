@extends('layouts.dashboard')


@section('body')

<nav aria-label="Page navigation example">
  <ul class="pagination">
    {{-- {{ dd($pex_response->per_page) }} --}}
    @if ($pex_response->page != 1)
     <li class="page-item"><a class="page-link" href="{{ Str::of($pex_response->prev_page)->replace('https://api.pexels.com/v1/search/', '/pexel/search/') }}">Previous</a></li>
        
    @endif
      <li class="page-item"><a class="page-link" href="{{ Str::of($pex_response->next_page)->replace('https://api.pexels.com/v1/search/', '/pexel/search/') }}">Next</a></li>
  </ul>
</nav>

<div>
  {{-- {{ dd($pex_response) }}  --}}

  <form action="/pexel/search" method="get">
    <input type="text" name="query" id="" placeholder="search">
    <input type="submit" value="search">
  </form>
</div>
<div class="row">
@foreach ($pex_response->photos as $photo)
    
<div class="col col-sm-12 col-lg-4">

  <div class="card" style="width: 18rem;">
    <img src="{{ $photo->src->medium }}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{ $photo->photographer }}</h5>
      <p class="card-text">
        <i class="text-danger">❤</i> by <a href="https://www.pexels.com" target="_blank">pexels</a>
      </p>
      <a href="{{ $photo->src->large2x }}" download class="btn btn-primary mb-1">Download large2x</a>
      <a href="https://www.pexels.com/photo/{{ $photo->id }}/download/" download class="btn btn-danger">Download original</a>
    </div>
  </div>

</div>
@endforeach
   



</div>

  <nav aria-label="Page navigation example">
    <ul class="pagination">
      {{-- {{ dd($pex_response->per_page) }} --}}
      @if ($pex_response->page != 1)
       <li class="page-item"><a class="page-link" href="{{ Str::of($pex_response->prev_page)->replace('https://api.pexels.com/v1/search/', '/pexel/search/') }}">Previous</a></li>
          
      @endif
        <li class="page-item"><a class="page-link" href="{{ Str::of($pex_response->next_page)->replace('https://api.pexels.com/v1/search/', '/pexel/search/') }}">Next</a></li>
    </ul>
  </nav>


@endsection