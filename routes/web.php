<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\facebookController;
use App\Http\Controllers\mediaController;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth','verified'])->group(function () {
    Route::get('/facebook/addpost', [facebookController::class, 'facebook_add_post']);
    Route::post('/facebook/store', [facebookController::class, 'facebook_store_post']);
    Route::get('/media/upload', [mediaController::class, 'media_page']);
    Route::post('/media/upload', [mediaController::class, 'media_store']);
    Route::post('/media/sort', [mediaController::class, 'media_sorter']);
    Route::get('/media/waiting', [mediaController::class, 'media_waiting_page']);
    Route::post('/media/waiting/delete', [mediaController::class, 'waiting_delete']);
    Route::post('/media/delete', [mediaController::class, 'media_delete']);
    Route::post('/media/pushnow', [mediaController::class, 'push_now']);
    Route::get('/pexel/search', [mediaController::class, 'test_pexel']);

});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
