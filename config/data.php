<?php
/*
see the documentation : https://developers.facebook.com/docs/pages/access-tokens/

To get a long-lived User access token you will first create
a short-lived User access token. Next, you will exchange the
short-lived User access token for a long-lived User access 
token. This token is valid for 60 days.

Send a GET request to the /oauth/access_token endpoint. 
Replace {app-id}, {app-secret}, and {access-token} with your information.

curl -i -X GET "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id={app-id}&client_secret={app-secret}&fb_exchange_token={short-lived-user-access-token}"


*/
return [
  'pre_page' => 15,
  'facebook_token' => '',
  'facebook_page' => '',
];

 ?>
