<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('FBPosts', function (Blueprint $table) {
            $table->id();
            $table->string('post')->nullable();
            $table->string('link')->nullable();
            $table->string('filename')->nullable();
            $table->string('filetype')->nullable();
            $table->boolean('status')->nullable();
            $table->string('error')->nullable();
            $table->string('post_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('timer')->nullable();
            $table->string('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
