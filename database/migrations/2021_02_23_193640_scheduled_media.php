<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ScheduledMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('message')->nullable();
            $table->string('filename')->nullable();
            $table->string('type')->nullable();
            $table->string('date')->nullable();
        });
    }
}
