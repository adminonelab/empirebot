<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Log;
class keepUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'keep up to date and watch posts to send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $facebook_token = config('data.facebook_token');
        $facebook_page = config('data.facebook_page');

        // dd($facebook_page);

        // get all posts to send
        $now = Carbon::now()->toDateTimeString();
        $all_posts = DB::table('FBPosts')
        // ->whereDate('timer','<=',$now)
        ->where('status',1)
        ->get();
        
    //    dd($all_posts);
        // loop over all posts        
        foreach ($all_posts as $post) {
            // dd($post->user_id);

            // get last user post information to check date and time
            $last_post = DB::table('lastUserPost')
            ->where('user_id',$post->user_id)
            ->first();

            // is this first post for user? 
            // if not check last post time else make first
            if (isset($last_post->lastPost)) {
                // get last time and add the limit of delay to check
                // is delay time done or not                
                $releasTime = Carbon::create($last_post->lastPost)->addMinutes(5);
                $is_now_unlocked = Carbon::now()->gt($releasTime);
                // dd($is_now_unlocked);

            }else{
                $is_now_unlocked = true;
            }

                
            // $is_now_unlocked = true; //test
            // is releas and unlocked
            if (isset($is_now_unlocked) && $is_now_unlocked) {
                if ($post->filetype != null) {
                    # code...
                    // sending photo
                    // dd(Storage::get('public/'.$post->filename));
                    if ($post->filetype == 'image') {
                        // dd($post->filetype);
                        # code...
                        $response = Http::attach(
                            'source', Storage::get('public/'.$post->filename), $post->filename,
                            // 'source', file_get_contents('/home/adminone/Downloads/pexels-thlil-2378127.jpg'), '2021-02-21T10:20.jpg',
                            )->post('https://graph.facebook.com/'.$facebook_page.'/photos',[
                                'access_token' => $facebook_token,
                                'message' => $post->post,
                        ]);
                    }
                    if ($post->filetype == 'video') {
                        # code...
                        $response = Http::attach(
                            'source', Storage::get('public/'.$post->filename), $post->filename,
                            // 'source', file_get_contents('/home/adminone/Downloads/pexels-thlil-2378127.jpg'), '2021-02-21T10:20.jpg',
                            )->post('https://graph.facebook.com/'.$facebook_page.'/videos',[
                                'access_token' => $facebook_token,
                                'message' => $post->post,
                                'file_size' => Storage::size('public/'.$post->filename),
                        ]);
                    }
                    }else {
                        // // send request
                        $response = Http::post('https://graph.facebook.com/'.$facebook_page.'/feed', [
                            'message' => $post->post,
                            // 'message' => "test page ".Str::random(40),
                            'access_token' => $facebook_token,
                        ]);
                    }

                // $response = Http::attach(
                //     // 'attachment', file_get_contents('/home/adminone/Downloads/pexels-thlil-2378127.jpg'), 'photo.jpg'
                //     'file1', file_get_contents('/home/adminone/insta/instagram-crawler/software.engineer.hub/video/2021-02-21T10:20.mp4'), '2021-02-21T10:20.mp4'
                // )->post('http://127.0.0.1:8080/');


                               

                $response = \json_decode($response->body());
                // dd($response);
                // is post posted successfuly
                if (isset($response->id)) {
                    echo "##################\n";
                    echo "message : ".$post->post."\n";
                    echo "id : ".$response->id."\n";
                    echo "##################\n";

                // insert log of last post time if this is first time
                $status = DB::table('lastUserPost')->insertOrIgnore([
                    [
                        'user_id' => $post->user_id,
                        'lastPost' => $now,
                    ]
                ]);

                // if inserted before update last post field
                if ($status == 0) {
                    # code...
                    // dd($post->user_id);
                    $res = DB::table('lastUserPost')
                    ->where('user_id', $post->user_id)
                    ->update(['lastPost' => $now]);
                    // dd($res);
                }

                // make post posted by turn status off
                DB::table('FBPosts')
                ->where('id', $post->id)
                ->update(['status' => 0]);
                
                }else{
                    dd($response);
                }


            }else {
                echo "not unlocked \n";
            }
        
            

        }
        // dd(($all_posts));
        // echo "maxmox";

        // $this->info('>>>successful');
        return 0;
    }
}
