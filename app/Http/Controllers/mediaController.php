<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class mediaController extends Controller
{
    //
    function media_page()
    {
        # code...
        return view('media.media_form');
    }

    function test_pexel(Request $request)
    {
        # code...
        $query = $request->input('query') != null ? $request->input('query') : 'random' ;
        $per_page = $request->input('per_page') != null ? $request->input('per_page') : '';
        $page = $request->input('page') != null ? $request->input('page') : 1;
        try {
            
            $pex_response = Http::withHeaders([
                'Authorization' => '563492ad6f9170000100000145202b94973b4146845ab251230d29bc',
                ])->get('https://api.pexels.com/v1/search/', [
                    'query' => $query,
                    'per_page' => $per_page,
                    'page' => $page
            ])->body();
            
        } catch (\Throwable $th) {
            //throw $th;
            return '<a href="/dashboard">dashboard</a><br><br><div style="color:red;">may be threris no internet connection</div>';
        }
            
        $pex_response = \json_decode($pex_response);

        return view('pexel',['pex_response' => $pex_response]);
    }

    function media_waiting_page()
    {
        # code...
        $allImages = DB::table('FBPosts')
      ->where('status',1)

      ->paginate(config('data.pre_page'));
        return view('media.waiting_list',['allImages'=>$allImages]);
    }

    function waiting_delete(Request $request)
    {
        # code...
        $media_id = $request->input('id');
        // dd($media_id);
        DB::table('post_timer')
        ->where('id',$media_id)
        ->delete();

        return back();
    }

    function media_delete(Request $request)
    {
        # code...
        $media_id = $request->input('id');
        // dd($media_id);
        DB::table('none_scheduled_media')
        ->where('id',$media_id)
        ->delete();

        return back();
    }

    function media_store(Request $request)
    {
        # code...
        foreach ($request->except('_token') as  $input_key => $input_value) {
            if ($request->hasFile($input_key)) {
                // dd($request->file('file1'));
                # code...
                // dd($request->except('_token'));

                // dd($input_key);
                // return $request->except('_token');
                $files = $request->file($input_key);
                
                foreach ($files as $file) {
                    $extension = $file->extension();
                    $white_list = array("image" => array('png','jpeg','jpg'),"video" => array('mp4','ogg','webm'));
                    if (in_array($extension,$white_list['image']) || in_array($extension,$white_list['video'])) {
                        # code...
                        $type = in_array($extension,$white_list['image']) == true ? $type = 'image' : $type = 'video';
                        $path = Storage::putFile('public', $file);
                        $filename = Str::of($path)->explode('/');

                        DB::table('none_scheduled_media')
                        ->insert([
                            'filename' => $filename[1],
                            'type' => $type,
                            'user_id' => Auth::user()->id,
                            'date' => Carbon::now()->toDateTimeString(),
                        ]);
                    }else {
                        dd('file type is not support');
                    }
                }
            }
            # code...
        }
        // dd($request->file('file1'));
        // dd($request->file('file1')->extension());
        // dd($request->hasFile('file1'));
        // dd($request->except('_token'));
        return back();
    }

    function media_sorter(Request $request)
    {
        $file_id = $request->input('id');
        $filename = $request->input('filename');
        $type = $request->input('type');
        $message = $request->input('post_message');
        // dd($type);
        DB::table('FBPosts')
        ->insert([
            'post' => $message,
            'filename' => $filename,
            'filetype' =>  $type,
            'user_id' => Auth::user()->id,

            'status' =>  1,
            'date' => Carbon::now()->toDateTimeString(),
        ]);

        DB::table('none_scheduled_media')
        ->where('id',$file_id)
        ->delete();
        
        return back();
        
    }

    function push_now(Request $request)
    {
        $file_id = $request->input('id');
        $facebook_token = config('data.facebook_token');
        $facebook_page = config('data.facebook_page');
        // dd($facebook_page,$facebook_token,config('data.facebook_page'));
        
        $post = DB::table('FBPosts')
        ->where('id',$file_id)
        ->first();
        // dd($post->id);
        // dd($post);
        if ($post->filetype != null) {

        if ($post->filetype == 'image') {
            // dd($post->filetype);
            # code...
            $facebook_response = Http::attach(
                'source', Storage::get('public/'.$post->filename), $post->filename,
                // 'source', file_get_contents('/home/adminone/Downloads/pexels-thlil-2378127.jpg'), '2021-02-21T10:20.jpg',
                )->post('https://graph.facebook.com/'.$facebook_page.'/photos',[
                    'access_token' => $facebook_token,
                    'message' => $post->post,
            ]);
        }
        if ($post->filetype == 'video') {
            # code...
            $facebook_response = Http::attach(
                'source', Storage::get('public/'.$post->filename), $post->filename,
                // 'source', file_get_contents('/home/adminone/Downloads/pexels-thlil-2378127.jpg'), '2021-02-21T10:20.jpg',
                )->post('https://graph.facebook.com/'.$facebook_page.'/videos',[
                    'access_token' => $facebook_token,
                    'message' => $post->post,
                    'file_size' => Storage::size('public/'.$post->filename),
            ]);
        }
        }else {
            // // send request
            $facebook_response = Http::post('https://graph.facebook.com/'.$facebook_page.'/feed', [
                'message' => $post->post,
                // 'message' => "test page ".Str::random(40),
                'access_token' => $facebook_token,
            ]);
        }
        $facebook_json = \json_decode($facebook_response->body());
        if (isset($facebook_json->id)) {
            // dd(isset($facebook_json->post_id),$facebook_json);
            DB::table('FBPosts')
            ->where('id', $post->id)
            ->update(['status' => 0]);
        }

        

        return back();
    }

}
