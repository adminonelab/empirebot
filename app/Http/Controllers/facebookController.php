<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class facebookController extends Controller
{
    //
    function facebook_add_post(){
        $allImages = DB::table('none_scheduled_media')
        
        ->paginate(config('data.pre_page'));

        return view('facebook.add_post',['allImages' => $allImages]);

    }

    function facebook_store_post(Request $request){
        $post_message = $request->input('post_message');
        $user_id = Auth::user()->id;
        $time = Carbon::now()->addMinute()->toDateTimeString();
        
        // dd($time,Carbon::now()->toDateTimeString());
        $status = DB::table('FBPosts')
        ->insert([
            'post'=>$post_message,
            'user_id' => $user_id,
            'status' => 1,
            'timer'=>$time
        ]);
        return back();
    }
}
